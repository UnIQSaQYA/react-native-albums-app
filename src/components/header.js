import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const Header = (props) => (
        <View style={styles.container}>
            <Text style={styles.textStyle}>{ props.headerText }</Text>
        </View>
    );

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8',
        padding: 20,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20
    }
});

export default Header;
